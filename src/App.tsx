import { useTranslation } from 'react-i18next';

import './App.css'

function App() {
  const { t, i18n } = useTranslation();

  const onClick = (lang: string) => {
    i18n.changeLanguage(lang)
  }

  return (
    <>
      <div>
      </div>
      <h1>{t('title')}</h1>
      <div className="card">
      <button onClick={() => onClick('en')}>
          English
        </button>
        <button style={{ marginLeft: 12 }} onClick={() => onClick('zhCn')}>
          中文（簡体）
        </button>
      </div>
      <input className='input' placeholder={t('')} />
    </>
  )
}

export default App
